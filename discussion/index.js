console.log("Happy Thursday!");

// An array in programming is simply a list of data

let studentNumberA = '2023-1923';
let studentNumberB = '2023-1924';
let studentNumberC = '2023-1925';
let studentNumberD = '2023-1926';
let studentNumberE = '2023-1927';

// with an array, we can simply write the like this:

let studentNumbers = ['2023-1923', '2023-1924', '2023-1925', '2023-1926', '2023-1927'];

// [SECTION] Arrays

/*
    - Arrays are used to store multiple related values in a single variable
    - They are declared using square brackets ([]) also known as "Array Literals"
    - Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
    - Arrays also provide access to a number of functions/methods that help in achieving this
    - A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
    - Majority of methods are used to manipulate information stored within the same object
    - Arrays are also objects which is another data type
    - The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
    - Syntax
        let/const arrayName = [elementA, elementB, Elem
- Syntax
        let/const arrayName = [elementA, elementB, ElementC...]
*/

// Common examples of arrays
 // use array of the same data types 

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenove', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
// Possible use of array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative ways to write arrays
let myTasks = [
		'drink HTML',
		'eat JavaScript',
		'inhale CSS',
		'bake SASS'
	];
console.log(myTasks);

// create an array with values from variables

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];

console.log(cities);

// [SECTION] Length Property
		//The .length property allows us to get and set the total number of items in an array.
console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// length property can also set the total number of items in array meaning we can delete the last item and shorten the array

myTasks.length = myTasks.length -1;
console.log(myTasks.length);
console.log(myTasks);

// Another example is using decrementation
cities.length--;
console.log(cities.length);
console.log(cities);

// we can also lengthen it by adding a number into the length property

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++;
console.log(theBeatles);

// [SECTION] Reading from Arrays
/*
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with it's own index/number
    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    - The reason an array starts with 0 is due to how the language is designed
- Array indexes actually refer to an address/location in the device's memory and how the information is stored
    - Example array location in memory
        Array address: 0x7ffe9472bad0
        Array[0] = 0x7ffe9472bad0
        Array[1] = 0x7ffe9472bad4
        Array[2] = 0x7ffe9472bad8
    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
- Syntax
        arrayName[index];
*/
console.log(grades[2]);
console.log(computerBrands[3]);
// Accessing an array element/item that does not exist will return "undefined"
console.log(grades[10]);

// length				1		2		3		4			5
// index				0		1		2		3			4		
let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
// access the second item in the array
console.log(lakersLegends[1]);
// access the fourth item on the array
console.log(lakersLegends[3]);

// You can also store / save array items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// You can also reassign array values using the items indices
console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = 'Pau Gasol';
console.log('Array after reassignment');
console.log(lakersLegends);

// Accessing the last item in the array

let lastIndexElement = lakersLegends.length - 1;

console.log(lakersLegends[lastIndexElement]);

// add it directly
console.log(lakersLegends[lakersLegends.length - 1]);

// adding items into the array
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barret Wallface";
console.log(newArr);

// Looping over an array
//You can use a for loop to iterate over all items in an array.
//Set the counter as the index and set a condition that as long as the current index iterated is less than the length of the array, we will run the condition. It is set this way because the index of an array starts at 0.

for(let index = 0; index < newArr.length; index++) {

	// newArr[0] = Tifa Lockhart
	// newArr[1] = Cloud Strife
	// newArr[2] = Barret Wallace
	console.log(newArr[index]);
};

for(let index = newArr.length - 1; index >=newArr.length; index--) {


	console.log(newArr[index]);
};

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++) {

	if(numArr[index] % 5 === 0) {
		console.log(numArr[index] + ' is divisible by 5')
	} else {
		console.log(numArr[index] + ' is not divisible by 5')
	}
}

// [SECTION] MultiDimensional Arrays

/*
    - Multidimensional arrays are useful for storing complex data structures
    - A practical application of this is to help visualize/create real world objects
    - Though useful in a number of cases, creating complex array structures is not always recommended.
*/
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
// Accessing elements of a multidimensional arrays
console.log(chessBoard[1][4]);

console.log("Pawn moves to: " + chessBoard[1][5]);